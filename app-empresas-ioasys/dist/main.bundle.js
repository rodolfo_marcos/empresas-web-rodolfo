webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ".logo{\r\n\theight: 50%;\r\n}\r\n\r\n/*Screen density logo*/\r\n\r\n@media only screen and (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (-webkit-min-device-pixel-ratio: 1.5), (min-device-pixel-ratio: 1.5) {\r\n\tspan{\r\n\t\twidth: 20px;\r\n\t}\r\n}\r\n"

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-toolbar color=\"primary\" class=\"main-toolbar\">\n\t<span class=\"spacer\"></span>\n\t<img src=\"assets/logo-home.png\" class=\"logo\">\n</mat-toolbar>\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'iOasys Rodolfo Teste';
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser_animations__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__material_material_module__ = __webpack_require__("./src/app/material/material.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__search_search_component__ = __webpack_require__("./src/app/search/search.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__login_login_component__ = __webpack_require__("./src/app/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ioasys_api_service__ = __webpack_require__("./src/app/ioasys-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__enterprise_show_enterprise_show_component__ = __webpack_require__("./src/app/enterprise-show/enterprise-show.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};












var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["K" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_8__search_search_component__["a" /* SearchComponent */],
                __WEBPACK_IMPORTED_MODULE_9__login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_11__enterprise_show_enterprise_show_component__["a" /* EnterpriseShowComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_4__material_material_module__["a" /* MaterialModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["d" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_forms__["i" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_router__["c" /* RouterModule */].forRoot([
                    { path: '', component: __WEBPACK_IMPORTED_MODULE_9__login_login_component__["a" /* LoginComponent */] },
                    { path: 'search', component: __WEBPACK_IMPORTED_MODULE_8__search_search_component__["a" /* SearchComponent */] },
                    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_9__login_login_component__["a" /* LoginComponent */] },
                    { path: 'search/:Id', component: __WEBPACK_IMPORTED_MODULE_11__enterprise_show_enterprise_show_component__["a" /* EnterpriseShowComponent */] }
                ])
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_10__ioasys_api_service__["a" /* IoasysApiService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/enterprise-show/enterprise-show.component.css":
/***/ (function(module, exports) {

module.exports = ".enterprise{\r\n\tpadding: 20px;\r\n\t-webkit-box-sizing: border-box;\r\n\t        box-sizing: border-box;\r\n}\r\n\r\n.enterprise img{\r\n\twidth: 100%;\r\n\tborder-radius: 15px;\r\n}\r\n\r\n@media only screen and (min-width: 800px) {\r\n\t.enterprise > div{\r\n\t\twidth: 45%;\r\n\t\tdisplay: inline-block;\r\n\t\tvertical-align: text-top;\r\n\t\tpadding: 20px;\r\n\t\t-webkit-box-sizing: border-box;\r\n\t\t        box-sizing: border-box;\r\n\t}\r\n}\r\n\r\n.close{\r\n\tposition: absolute;\r\n\ttop: 0px;\r\n\tfont-size: 40px;\r\n\tright: 21px;\r\n\tcolor: #e75680;\r\n\tcursor: pointer;\r\n}"

/***/ }),

/***/ "./src/app/enterprise-show/enterprise-show.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-icon class=\"close\" (click)=\"close()\" color=\"accent\">close</mat-icon>\n<div class=\"mat-typography enterprise\">\n\t<div>\n\t\t<h3>{{enterprise.enterprise_name}}</h3>\n\t\t<img src=\"http://54.94.179.135:8090/{{enterprise.photo}}\">\n\t</div>\n\t<div>\n\t\t<p>{{enterprise.city}} - {{enterprise.country}}</p>\n\t\t<p>{{enterprise.description}}</p>\n\t\t<p>Phone: {{enterprise.phone}}</p>\n\t</div>\n</div>"

/***/ }),

/***/ "./src/app/enterprise-show/enterprise-show.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EnterpriseShowComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ioasys_api_service__ = __webpack_require__("./src/app/ioasys-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common__ = __webpack_require__("./node_modules/@angular/common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EnterpriseShowComponent = /** @class */ (function () {
    function EnterpriseShowComponent(_location, route, ioasysApiService, router) {
        this._location = _location;
        this.route = route;
        this.ioasysApiService = ioasysApiService;
        this.router = router;
        this.enterpriseID = '';
        this.enterprise = {};
    }
    EnterpriseShowComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (!this.ioasysApiService.isLogged) {
            this.router.navigate(['login']);
        }
        this.route.paramMap
            .subscribe(function (params) {
            _this.enterpriseID = params.get('Id');
            _this.getEnterprise();
        });
    };
    EnterpriseShowComponent.prototype.close = function () {
        this._location.back();
    };
    EnterpriseShowComponent.prototype.getEnterprise = function () {
        var _this = this;
        this.ioasysApiService.getEnterprise(this.enterpriseID)
            .subscribe(function (data) {
            _this.enterprise = data['enterprise'];
            console.log(data);
        }, function (error) {
            console.log(error);
        });
    };
    EnterpriseShowComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-enterprise-show',
            template: __webpack_require__("./src/app/enterprise-show/enterprise-show.component.html"),
            styles: [__webpack_require__("./src/app/enterprise-show/enterprise-show.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_common__["f" /* Location */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_1__ioasys_api_service__["a" /* IoasysApiService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], EnterpriseShowComponent);
    return EnterpriseShowComponent;
}());



/***/ }),

/***/ "./src/app/ioasys-api.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IoasysApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var IoasysApiService = /** @class */ (function () {
    function IoasysApiService(http) {
        this.http = http;
        this.loginURL = 'http://54.94.179.135:8090/api/v1/users/auth/sign_in';
        this.enterprisesURL = 'http://54.94.179.135:8090/api/v1/enterprises';
        this.isLogged = false;
        this.accessToken = '';
        this.client = '';
        this.uid = '';
    }
    IoasysApiService.prototype.login = function (email, password) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]('Content-Type:application/json');
        return this.http.post(this.loginURL, JSON.stringify({ email: email, password: password }), { headers: headers, observe: 'response' });
    };
    IoasysApiService.prototype.getEnterprieses = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]();
        headers = headers.set('Content-Type', 'application/json');
        headers = headers.set('access-token', this.accessToken);
        headers = headers.set('client', this.client);
        headers = headers.set('uid', this.uid);
        return this.http.get(this.enterprisesURL, { headers: headers });
    };
    IoasysApiService.prototype.getEnterprise = function (id) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]();
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpParams */]();
        headers = headers.set('Content-Type', 'application/json');
        headers = headers.set('access-token', this.accessToken);
        headers = headers.set('client', this.client);
        headers = headers.set('uid', this.uid);
        return this.http.get(this.enterprisesURL + "/" + id, { headers: headers });
    };
    IoasysApiService.prototype.searchEnterprieses = function (name, types) {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["c" /* HttpHeaders */]();
        var params = new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpParams */]();
        headers = headers.set('Content-Type', 'application/json');
        headers = headers.set('access-token', this.accessToken);
        headers = headers.set('client', this.client);
        headers = headers.set('uid', this.uid);
        if (name) {
            params = params.set('name', name);
        }
        if (types) {
            params = params.set('enterprise_types', types);
        }
        return this.http.get(this.enterprisesURL, { headers: headers, params: params });
    };
    IoasysApiService.prototype.setAuthenticationParameters = function (accessToken, client, uid) {
        this.accessToken = accessToken;
        this.client = client;
        this.uid = uid;
        this.isLogged = true;
    };
    IoasysApiService.prototype.saveAutoLogin = function (email, password) {
        sessionStorage.setItem('email', email);
        sessionStorage.setItem('password', password);
    };
    IoasysApiService.prototype.getAutoLogin = function () {
        var email = sessionStorage.getItem('email');
        var password = sessionStorage.getItem('password');
        if (email && password) {
            return {
                email: email,
                password: password
            };
        }
        return null;
    };
    IoasysApiService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["C" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], IoasysApiService);
    return IoasysApiService;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/***/ (function(module, exports) {

module.exports = "form{\r\n\ttext-align: center;\r\n\tmargin-top: 100px;\r\n}\r\n\r\n.remember{\r\n\tdisplay: block;\r\n\tmargin: 15px auto;\r\n\tposition: relative;\r\n}\r\n\r\n@media only screen and (min-width: 800px) {\r\n\tform{\r\n\t\tmargin-top: 150px;\r\n\t}\r\n\r\n\tform .form-item{\r\n\t\twidth: 60%;\r\n\t}\r\n}"

/***/ }),

/***/ "./src/app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"mat-typography\" [formGroup]=\"loginForm\" (ngSubmit)=\"login(loginForm.value)\">\n\t\n\t<h1>BEM VINDO AO EMPRESAS</h1>\n\t<p>Este é o teste de Rodolfo, usando Angular 5 e Material Design</p>\n  \t\t\n\t<mat-form-field class=\"form-item\">\n\t\t<mat-icon matPrefix (click)=\"filterElements()\">mail</mat-icon>\n\t\t<input matInput placeholder=\"E-mail\" type=\"text\" formControlName=\"email\">\n\t\t<mat-hint align=\"end\">Obrigatório</mat-hint>\n\t</mat-form-field>\n\t\n\t<mat-form-field class=\"form-item\">\n\t\t<mat-icon matPrefix (click)=\"filterElements()\">lock</mat-icon>\n\t\t<input matInput placeholder=\"Senha\" type=\"password\" formControlName=\"password\">\n\t\t<mat-hint align=\"end\">Obrigatório</mat-hint>\n\t</mat-form-field>\n\n\t<div class=\"remember\">\n\t\t<mat-slide-toggle matInput formControlName=\"remember\">Entrar automaticamente</mat-slide-toggle>\n\t</div>\n\n\t<!-- <mat-slide-toggle matInput formControlName=\"isMobile\">Mobile</mat-slide-toggle> -->\n\n\t<br>\n\t<button mat-raised-button type=\"submit\" color=\"accent\" [disabled]=\"!loginForm.valid\">Entrar</button>\n\n</form>"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ioasys_api_service__ = __webpack_require__("./src/app/ioasys-api.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginComponent = /** @class */ (function () {
    function LoginComponent(fb, ioasysApiService, router) {
        this.fb = fb;
        this.ioasysApiService = ioasysApiService;
        this.router = router;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.loginForm = this.fb.group({
            "email": [null, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["j" /* Validators */].required],
            "password": [null, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["j" /* Validators */].required],
            "remember": false
        });
        var autoLogin = this.ioasysApiService.getAutoLogin();
        if (autoLogin) {
            this.loginForm.controls['remember'].setValue(true);
            this.tryLogin(autoLogin['email'], autoLogin['password'], false);
        }
    };
    LoginComponent.prototype.login = function (value) {
        this.tryLogin(value.email, value.password, value.remember);
    };
    LoginComponent.prototype.tryLogin = function (email, password, remember) {
        var _this = this;
        this.ioasysApiService.login(email, password)
            .subscribe(function (data) {
            console.log(data);
            if (data['body']['success']) {
                var accessToken = data['headers'].get('access-token');
                var client = data['headers'].get('client');
                var uid = data['headers'].get('uid');
                if (remember) {
                    _this.ioasysApiService.saveAutoLogin(email, password);
                }
                _this.ioasysApiService.setAuthenticationParameters(accessToken, client, uid);
                _this.router.navigate(['search']);
            }
        }, function (error) {
            alert(error.statusText);
            console.log(error);
        });
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-login',
            template: __webpack_require__("./src/app/login/login.component.html"),
            styles: [__webpack_require__("./src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_3__ioasys_api_service__["a" /* IoasysApiService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/material/material.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MaterialModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__("./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["K" /* NgModule */])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_material__["b" /* MatCardModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["l" /* MatMenuModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["c" /* MatCheckboxModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["d" /* MatChipsModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["u" /* MatTooltipModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["s" /* MatTableModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["p" /* MatSelectModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["m" /* MatOptionModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["n" /* MatProgressBarModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["e" /* MatDividerModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["i" /* MatIconModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MatButtonModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["t" /* MatToolbarModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["q" /* MatSidenavModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["g" /* MatFormFieldModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["j" /* MatInputModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["r" /* MatSlideToggleModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["k" /* MatListModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["f" /* MatExpansionModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["o" /* MatProgressSpinnerModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["h" /* MatGridListModule */]],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_material__["b" /* MatCardModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["l" /* MatMenuModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["c" /* MatCheckboxModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["d" /* MatChipsModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["u" /* MatTooltipModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["s" /* MatTableModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["p" /* MatSelectModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["m" /* MatOptionModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["n" /* MatProgressBarModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["e" /* MatDividerModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["i" /* MatIconModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["a" /* MatButtonModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["t" /* MatToolbarModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["q" /* MatSidenavModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["g" /* MatFormFieldModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["j" /* MatInputModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["r" /* MatSlideToggleModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["k" /* MatListModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["f" /* MatExpansionModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["o" /* MatProgressSpinnerModule */], __WEBPACK_IMPORTED_MODULE_1__angular_material__["h" /* MatGridListModule */]]
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/search/search.component.css":
/***/ (function(module, exports) {

module.exports = ".searchBar{\r\n\tposition: fixed;\r\n\tz-index: 9;\r\n\twidth: 100%;\r\n\tbackground: #fff;\r\n\ttop: 0px;\r\n\tpadding: 0% 5%;\r\n\t-webkit-box-sizing: border-box;\r\n\t        box-sizing: border-box;\r\n}\r\n\r\n.noResult{\r\n\tmargin-top: 150px;\r\n\ttext-align: center;\r\n}\r\n\r\nmat-card{\r\n\tmargin: 12px;\r\n\t-webkit-box-sizing: border-box;\r\n\t        box-sizing: border-box;\r\n}\r\n\r\n@media only screen and (max-width: 800px) {\r\n\tmat-form-field{\r\n\t\twidth: 45%;\r\n\t\tdisplay: inline-block;\r\n\t\t-webkit-box-sizing: border-box;\r\n\t\t        box-sizing: border-box;\r\n\t}\r\n}\r\n\r\n@media only screen and (min-width: 800px) {\r\n\tmat-card{\r\n\t\twidth: 300px;\r\n\t\tdisplay: inline-block;\r\n\t\tmargin: 10px;\r\n\t\tvertical-align: text-top;\r\n\t}\r\n}"

/***/ }),

/***/ "./src/app/search/search.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"searchBar\">\n\t<form  [formGroup]=\"searchForm\">\n\t\t<mat-form-field>\n\t\t\t<input type=\"text\" matInput placeholder=\"Busca por empresas\" formControlName=\"searchText\">\n\t\t</mat-form-field>\n\t\t<mat-form-field>\n\t\t\t<mat-select placeholder=\"Todos os tipos\" formControlName=\"searchType\">\n\t\t\t\t<mat-option *ngFor=\"let type of types\" [value]=\"type.id\">{{type.enterprise_type_name}}</mat-option>\n\t\t\t</mat-select>\n\t\t</mat-form-field>\n\t\t<mat-icon matSuffix (click)=\"filterElements()\">search</mat-icon>\n\t</form>\n</div>\n<div class=\"noResult mat-typography\" *ngIf=\"!enterprises.length\">\n\t<h4>Ops, nenhum resultado encontrado.</h4>\n</div>\n<div class=\"searchResult\" *ngIf=\"enterprises.length\">\n\t<mat-card class=\"example-card\" *ngFor=\"let enterprise of enterprises\">\n\t\t<mat-card-header>\n\t\t\t<mat-card-title>{{enterprise.enterprise_name}}</mat-card-title>\n\t\t\t<mat-card-subtitle>{{enterprise.city}} - {{enterprise.country}}</mat-card-subtitle>\n\t\t</mat-card-header>\n\t\t<img mat-card-image src=\"http://54.94.179.135:8090/{{enterprise.photo}}\" alt=\"Photo of a Shiba Inu\">\n\t\t<mat-card-content>\n\t\t\t<p>\n\t\t\t\t{{enterprise.description}}\n\t\t\t</p>\n\t\t</mat-card-content>\n\t\t<mat-card-actions>\n\t\t\t<button [routerLink]=\"['/search', enterprise.id]\" mat-button>DETALHES</button>\n\t\t</mat-card-actions>\n\t</mat-card>\n</div>"

/***/ }),

/***/ "./src/app/search/search.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ioasys_api_service__ = __webpack_require__("./src/app/ioasys-api.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SearchComponent = /** @class */ (function () {
    function SearchComponent(fb, ioasysApiService, router) {
        this.fb = fb;
        this.ioasysApiService = ioasysApiService;
        this.router = router;
        this.filterType = [];
        this.searchText = '';
        this.enterprises = [];
        this.types = [];
    }
    SearchComponent.prototype.ngOnInit = function () {
        this.searchForm = this.fb.group({
            "searchText": "",
            "searchType": []
        });
        if (!this.ioasysApiService.isLogged) {
            this.router.navigate(['login']);
        }
        this.search("");
    };
    SearchComponent.prototype.filterElements = function () {
        var _this = this;
        console.log(this.searchForm.value);
        this.ioasysApiService.searchEnterprieses(this.searchForm.value.searchText, this.searchForm.value.searchType)
            .subscribe(function (data) {
            console.log(data);
            _this.enterprises = data['enterprises'];
        }, function (error) {
            console.log(error);
        });
    };
    SearchComponent.prototype.search = function (value) {
        var _this = this;
        this.ioasysApiService.getEnterprieses()
            .subscribe(function (data) {
            console.log(data);
            _this.enterprises = data['enterprises'];
            _this.types = _this.enterprises.map(function (e) { return e.enterprise_type; });
            var flags = {};
            _this.types = _this.types.filter(function (type) {
                if (flags[type.id]) {
                    return false;
                }
                flags[type.id] = true;
                return true;
            });
        }, function (error) {
            console.log(error);
        });
    };
    SearchComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-search',
            template: __webpack_require__("./src/app/search/search.component.html"),
            styles: [__webpack_require__("./src/app/search/search.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2__ioasys_api_service__["a" /* IoasysApiService */], __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]])
    ], SearchComponent);
    return SearchComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_17" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map