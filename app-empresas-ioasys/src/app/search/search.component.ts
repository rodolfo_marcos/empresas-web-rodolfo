import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { IoasysApiService } from '../ioasys-api.service';
import { Router } from "@angular/router";

@Component({
	selector: 'app-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.css']
})

export class SearchComponent implements OnInit {

	searchForm: FormGroup;
	filterType = [];
	searchText = '';
	enterprises = <any>[];
	types = <any>[];

	constructor(private fb: FormBuilder, private ioasysApiService: IoasysApiService, private router: Router) { }

	ngOnInit() {
		this.searchForm = this.fb.group({
			"searchText": "",
			"searchType": []
		});
		if(!this.ioasysApiService.isLogged){
			this.router.navigate(['login']);
		}
		this.search("");
	}

	filterElements(){
		console.log(this.searchForm.value);
		this.ioasysApiService.searchEnterprieses(this.searchForm.value.searchText,this.searchForm.value.searchType)
			.subscribe(data => {
				console.log(data);
				this.enterprises = data['enterprises'];
			}, error => {
				console.log(error);
			})
	}

	search(value){
		this.ioasysApiService.getEnterprieses()
			.subscribe(data => {
				console.log(data);
				this.enterprises = data['enterprises'];
				this.types = this.enterprises.map(e => e.enterprise_type);
				var flags = {};
				this.types = this.types.filter(function(type) {
					if (flags[type.id]) {
						return false;
					}
					flags[type.id] = true;
					return true;
				});
			}, error => {
				console.log(error);
			})
	}

}
