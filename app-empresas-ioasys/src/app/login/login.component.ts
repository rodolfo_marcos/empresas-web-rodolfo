import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from "@angular/router";
import { IoasysApiService } from '../ioasys-api.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	loginForm: FormGroup;

	constructor(private fb: FormBuilder, private ioasysApiService: IoasysApiService, private router: Router) { }

	ngOnInit() {
		this.loginForm = this.fb.group({
			"email": [null, Validators.required],
			"password": [null, Validators.required],
			"remember": false
		});

		var autoLogin = this.ioasysApiService.getAutoLogin();
		if(autoLogin){
			this.loginForm.controls['remember'].setValue(true);
			this.tryLogin(autoLogin['email'], autoLogin['password'], false);
		}
	}

	login(value:any){
		this.tryLogin(value.email, value.password, value.remember);
	}

	tryLogin(email: string, password: string, remember: boolean){
		this.ioasysApiService.login(email, password)
			.subscribe(data => {
				console.log(data);
				if(data['body']['success']){
					var accessToken = data['headers'].get('access-token');
					var client = data['headers'].get('client');
					var uid = data['headers'].get('uid');
					if(remember){
						this.ioasysApiService.saveAutoLogin(email, password);
					}
					this.ioasysApiService.setAuthenticationParameters(accessToken, client, uid);
					this.router.navigate(['search']);
				}
			}, error => {
				alert(error.statusText);
				console.log(error);
			});
	}

}
