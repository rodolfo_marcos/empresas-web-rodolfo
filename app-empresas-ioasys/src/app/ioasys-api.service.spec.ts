import { TestBed, inject } from '@angular/core/testing';

import { IoasysApiService } from './ioasys-api.service';

describe('IoasysApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IoasysApiService]
    });
  });

  it('should be created', inject([IoasysApiService], (service: IoasysApiService) => {
    expect(service).toBeTruthy();
  }));
});
