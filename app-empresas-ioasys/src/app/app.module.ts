import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { LoginComponent } from './login/login.component';
import { IoasysApiService} from './ioasys-api.service';
import { EnterpriseShowComponent } from './enterprise-show/enterprise-show.component'

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    LoginComponent,
    EnterpriseShowComponent
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {path: '', component: LoginComponent},
      {path: 'search', component: SearchComponent},
      {path: 'login', component: LoginComponent},
      {path: 'search/:Id', component: EnterpriseShowComponent}
    ])
  ],
  providers: [IoasysApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
