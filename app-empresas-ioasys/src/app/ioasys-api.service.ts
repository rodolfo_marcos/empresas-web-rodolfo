import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

@Injectable()
export class IoasysApiService {

	loginURL = 'http://54.94.179.135:8090/api/v1/users/auth/sign_in';
	enterprisesURL = 'http://54.94.179.135:8090/api/v1/enterprises';

	public isLogged = false;

	accessToken = '';
	client = '';
	uid = '';

	constructor(public http: HttpClient) { }

	login(email: string, password: string): Observable<Object>{
		var headers = new HttpHeaders('Content-Type:application/json');
		return this.http.post(this.loginURL, JSON.stringify({email: email, password: password}), {headers: headers, observe:'response'});
	}

	getEnterprieses(): Observable<Object>{
		var headers = new HttpHeaders();
		headers = headers.set('Content-Type','application/json');
		headers = headers.set('access-token',this.accessToken);
		headers = headers.set('client',this.client);
		headers = headers.set('uid',this.uid);
		return this.http.get(this.enterprisesURL, {headers: headers});
	}

	getEnterprise(id: string): Observable<Object>{
		var headers = new HttpHeaders();
		var params = new HttpParams();
		headers = headers.set('Content-Type','application/json');
		headers = headers.set('access-token',this.accessToken);
		headers = headers.set('client',this.client);
		headers = headers.set('uid',this.uid);
		return this.http.get(`${this.enterprisesURL}/${id}`, {headers: headers});
	}

	searchEnterprieses(name:string, types:string): Observable<Object>{
		var headers = new HttpHeaders();
		var params = new HttpParams();
		headers = headers.set('Content-Type','application/json');
		headers = headers.set('access-token',this.accessToken);
		headers = headers.set('client',this.client);
		headers = headers.set('uid',this.uid);
		if(name){
			params = params.set('name', name);
		}
		if(types){
			params = params.set('enterprise_types', types);
		}
		return this.http.get(this.enterprisesURL, {headers: headers, params: params});
	}

	setAuthenticationParameters(accessToken:string, client:string, uid:string){
		this.accessToken = accessToken;
		this.client = client;
		this.uid = uid;
		this.isLogged = true;
	}

	saveAutoLogin(email: string, password: string){
		sessionStorage.setItem('email',email);
		sessionStorage.setItem('password', password);
	}

	getAutoLogin(): object{
		var email = sessionStorage.getItem('email');
		var password = sessionStorage.getItem('password');
		if(email && password){
			return {
				email: email,
				password: password
			}
		}
		return null;
	}

}
