import { Component, OnInit } from '@angular/core';
import { IoasysApiService } from '../ioasys-api.service';
import { Router } from "@angular/router";
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-enterprise-show',
	templateUrl: './enterprise-show.component.html',
	styleUrls: ['./enterprise-show.component.css']
})
export class EnterpriseShowComponent implements OnInit {

	enterpriseID = '';
	enterprise = <any>{}; 

	constructor(private _location: Location, private route: ActivatedRoute, private ioasysApiService: IoasysApiService, private router: Router) { }

	ngOnInit() {
		if(!this.ioasysApiService.isLogged){
			this.router.navigate(['login']);
		}
		this.route.paramMap
		.subscribe(params => {
			this.enterpriseID = params.get('Id');
			this.getEnterprise();
		})
	}

	close(){
		this._location.back();
	}

	getEnterprise(){
		this.ioasysApiService.getEnterprise(this.enterpriseID)
		.subscribe(data => {
			this.enterprise = data['enterprise'];
			console.log(data);
		}, error => {
			console.log(error);
		})
	}

}
