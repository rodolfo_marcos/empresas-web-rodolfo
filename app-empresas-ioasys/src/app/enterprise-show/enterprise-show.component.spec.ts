import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterpriseShowComponent } from './enterprise-show.component';

describe('EnterpriseShowComponent', () => {
  let component: EnterpriseShowComponent;
  let fixture: ComponentFixture<EnterpriseShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterpriseShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterpriseShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
