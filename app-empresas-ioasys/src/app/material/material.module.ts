import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { 
	MatMenuModule, 
	MatIconModule, 
	MatButtonModule, 
	MatToolbarModule, 
	MatSidenavModule, 
	MatFormFieldModule, 
	MatInputModule, 
	MatSlideToggleModule, 
	MatListModule, 
	MatExpansionModule, 
	MatProgressSpinnerModule,
	MatGridListModule,
	MatDividerModule,
	MatProgressBarModule,
	MatTableModule,
	MatOptionModule,
	MatSelectModule,
	MatTooltipModule,
	MatChipsModule,
	MatCheckboxModule,
	MatCardModule
} from '@angular/material';

@NgModule({
	imports: [ MatCardModule, MatMenuModule, MatCheckboxModule, MatChipsModule, MatTooltipModule, MatTableModule, MatSelectModule, MatOptionModule, MatProgressBarModule, MatDividerModule, MatIconModule, MatButtonModule, MatToolbarModule, MatSidenavModule, MatFormFieldModule, MatInputModule, MatSlideToggleModule, MatListModule, MatExpansionModule, MatProgressSpinnerModule, MatGridListModule],
	exports: [ MatCardModule, MatMenuModule, MatCheckboxModule, MatChipsModule, MatTooltipModule, MatTableModule, MatSelectModule,  MatOptionModule, MatProgressBarModule, MatDividerModule, MatIconModule, MatButtonModule, MatToolbarModule, MatSidenavModule, MatFormFieldModule, MatInputModule, MatSlideToggleModule, MatListModule, MatExpansionModule, MatProgressSpinnerModule, MatGridListModule]
})

export class MaterialModule { }